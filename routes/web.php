<?php

use App\Http\Controllers\login;
use App\Http\Controllers\mahasiswa;
use App\Http\Controllers\dosen;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/test', [dosen::class,'index']);

// DOSEN

Route::get('/logindosen', [login::class,'formloginDosen'])->name('logindosen');
Route::post('/authlogindosen', [login::class,'authloginDosen'])->name('authlogindosen');
Route::get('/dosenhome', [dosen::class,'index'])->name('landing');
Route::get('/dosenprofile', [dosen::class,'profile'])->name('profiledosen');
Route::get('/dosenprofileedit', [dosen::class,'edit'])->name('editprofiledosen');
Route::post('/updateprofile/{id}',[dosen::class,'update'])->name('updateprofile');
Route::get('/dosenlist', [dosen::class,'daftardosen'])->name('daftardosen');
Route::get('/showeditriwayat', [dosen::class,'editriwayatdosen'])->name('editriwayat');
Route::get('/showtambahriwayat', [dosen::class,'tambahriwayatdosen'])->name('tambahriwayat');


// MAHASISWA

Route::get('/loginmahasiswa', [login::class,'formloginMhs'])->name('loginmhs');
Route::post('/authloginmahasiswa', [login::class,'authloginMhs'])->name('authloginmhs');
Route::get('/mhshome', [mahasiswa::class,'index'])->name('landingmhs');
Route::get('/mhsprofile', [mahasiswa::class,'profile'])->name('profilemhs');
// Route::get('/mhsmenu', [mahasiswa::class,'menu'])->name('editriwayat');
// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');
