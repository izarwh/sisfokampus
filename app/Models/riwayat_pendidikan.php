<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class riwayat_pendidikan extends Model
{
    use HasFactory;

    protected $table = 'riwayat_pendidikan';

    protected $fillable = ['strata','jurusan','sekolah','tahunMulai','tahunSelesai'];

    public $timestamps = false;

    public function dosen(){
        return $this->belongsTo('App\Models\dosen');
    }
};
