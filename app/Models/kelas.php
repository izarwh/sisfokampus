<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kelas extends Model
{
    use HasFactory;

    protected $table = 'kelas';

    protected $fillable = ['namaKelas'];

    protected $primaryKey = 'namaKelas';

    public $timestamps = false;

    public function kelas(){
        return $this->belongsToMany('App\Models\dosen');
    }
}
