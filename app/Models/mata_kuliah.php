<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mata_kuliah extends Model
{
    use HasFactory;

    protected $table = 'mata_kuliah';

    protected $fillable = ['id','namaMataKuliah','jumlahSKS'];

    public $timestamps = false;

    protected $primaryKey = 'ID';

    public function kelas(){
        return $this->belongsToMany('App\Models\kelas');
    }
}
