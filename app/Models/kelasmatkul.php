<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kelasmatkul extends Model
{
    use HasFactory;

    protected $table = 'kelasmatkul';

    protected $fillable = ['id','idkelas','idmatkul'];

    protected $primaryKey = 'id';

    public $imestamps = false;
}
