<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class dosen extends Authenticatable
{
    use HasFactory;

    protected $guard ='dosenuser';

    protected $table = 'dosen';

    protected $fillable = ['nip','nama','gelar','riwayat_pendidikan','password'];

    protected $primaryKey = 'nip';

    public $timestamps = false;

    public function riwayat_pendidikan(){
        return $this->hasmany('App\Models\riwayat_pendidikan');
    }

    public function mata_kuliah(){
        return $this->belongsToMany('App\Models\mata_kuliah');
    }
}
