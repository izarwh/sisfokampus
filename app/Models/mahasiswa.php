<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class mahasiswa extends Authenticatable
{
    use HasFactory;

    protected $guard = 'mahasiswauser';

    protected $table = 'mahasiswa';

    protected $fillable = ['NIM','nama','jenis_kelamin','TTL','password'];

    protected $primaryKey = 'NIM';

    public $timestamps = false;

    public function mata_kuliah(){
        return $this->belongsToMany('App\Models\matakuliah');
    }
}
