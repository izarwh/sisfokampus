<?php

namespace App\Http\Controllers;

use App\Models\dosen as ModelsDosen;
use App\Models\riwayat_pendidikan as ModelRiwayat;
use BaconQrCode\Common\Mode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class dosen extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
        // $data = DB::table('dosen')->get();
        $data = ModelsDosen::paginate();
        $id = session('user');
        $user = ModelsDosen::where('nip', $id)->first();
        // echo $id;
        return view('Dosen.landing',compact('user','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function profile(){
        $id = session('user');
        $user = ModelsDosen::where('nip',$id)->first();
        $riwayat = ModelRiwayat::where('nip',$id)->get();
        return view('Dosen.profiledosen',compact('user','riwayat'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $id = session('user');
        $user = ModelsDosen::where('nip',$id)->first();
        return view('Dosen.editprofile',['user'=> $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo $id;
        ModelsDosen::where('nip', $id)->update([
            'nama' => $request->nama,
            'gelar' => $request->gelar
        ]);
        return redirect()->back()->with('success','Data berhasil diganti');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function tambahriwayatdosen(){
        $id = session('user');
        $user = ModelsDosen::where('nip',$id)->first();
        return view('Dosen.tambahriwayat',['user'=>$user]);
    }
    public function editriwayatdosen(){
        return view('Dosen.editriwayat');
    }
}
