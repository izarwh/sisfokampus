<?php

namespace App\Http\Controllers;

use App\Models\dosen;
use GuzzleHttp\Psr7\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use App\User;

class login extends Controller
{
    // public function __construct() {
    //     $this->middleware('guest:dosenuser', ['except' => 'logout']);
    //     $this->middleware('guest:mahasiswauser', ['except' => 'logout']);
    // }

    public function formloginDosen(){
        return view('Dosen.logindosen');
    }

    public function formloginMhs(){
        return view('Mahasiswa.loginmahasiswa');
    }


    public function authloginDosen(Request $request){
        $auth1 = dosen::where('nip',$request->nip)->first();
        // echo $auth1;
        if ($auth1){
            $auth2 = dosen::where('password',$request->password)->first();
            if ($auth2){
                $request->session()->put('user', $request->nip);
                echo $id = $request->session()->get('user');
                return redirect('/dosenhome');
            }
            else{
                return redirect('/logindosen')->with('error','NIP atau password salah');
            }
        }else{
            return redirect('/logindosen')->with('error','NIP atau password salah');
        }
    }

    public function authloginMhs(Request $request){

    }

    public function authmiddleware(Request $request){
        $rules = [
            'nip' => 'required',
            'password' => 'required|string'
        ];

        $messages = [
            'nip.required' => 'NIP Perlu diisi',
            'password.required' => 'Password perlu diisi'
        ];

        // echo ($request->input('nip'));
        $validator = Validator::make($request->all(), $rules, $messages);

        $this->validate($request,[
            'nip' => 'required',
            'password' => 'required|string'
        ]);

        $data = [
                'nip' => $request->input('nip'),
                'password' => $request->input('password')
            ];

        if (Auth::guard('dosenuser')
        ->attempt($data)){
            $user = auth()->user();
            dd($user);
        }else{
            $user = Auth::user();
            dd($user);
            // return redirect()->back()->with('error','NIP atau password salah');
        }

    }
};
