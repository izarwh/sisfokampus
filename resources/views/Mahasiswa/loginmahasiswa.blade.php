<!DOCTYPE html>
<html lang="en">
<head>
    <link href="{{ asset('css/loginmhs.css') }}" rel="stylesheet" type="text/css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Mahasiswa</title>
</head>
<body>
    <div class="container">
        <div class="container">
            <div class="row content">
                <center>
                    <div class="class-md-6">
                        <img class="login__images" src="{{ asset('img/573.jpg') }}" alt="">
                    </div>
                </center>
                <div class="col-md-3">
                    <h3 class="signin-text mb-3">Log in Mahasiswa</h3>
                    <div class="form-group">
                        <label for="NIM">NIM</label>
                        <div class="col-2">
                            <input type="Text" name="NIM">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Nama">Password</label>
                        <div class="col-2">
                            <input type="Text" name="Password">
                        </div>
                    </div>
                    <a class="btn-info" type="form">log in</a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>