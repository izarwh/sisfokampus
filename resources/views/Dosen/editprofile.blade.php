@extends('templatedosen.navbar2')
@section('title','Sistem Informasi Kampus')
@section('content')
    <div class="container content">
            <div class="card edit col-4">
              <center>
                <form method="post" action="{{ route('updateprofile',['id' => $user]) }}">
                    @csrf
                    <div class="form-group col">
                      <label for="NIP">NIP</label>
                      <input type="text" class="form-control" value="{{ $user->nip }}" disabled>
                    </div>
                    <div class="form-group col">
                      <label for="Name">Nama</label>
                      <input type="text" class="form-control" name="nama" placeholder="Nama" value="{{ $user->nama }}">
                    </div>
                    <div class="form-group col">
                      <label for="Gelar">Gelar</label>
                      <input type="text" class="form-control" name="gelar" placeholder = "Gelar" value="{{ $user->gelar }}">
                    </div>
                    <button type="submit" class="btn btn-primary submit__btn col-3">Submit</button>
                    <a class="btn btn-danger submit__btn" href="{{ route('profiledosen') }}">Cancel</a>
                  </form>
                    @if (Session::has('success'))
                        <div class="alert alert-info">
                            {{ Session::get('success') }}
                        </div>
                    @endif
                </center>
            </div>
    </div>
@endsection