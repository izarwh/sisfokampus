@extends('templatedosen.navbar2')
@section('title','Penambahan Riwayat Dosen')
@section('content')
    <div class="container content">
        <center>
            <div class=" card col-4 ">
                <!-- Form code begins -->
                <form method="post" action="#">
                    <div class="form-group">
                        <label for="Universitas">Universitas</label>
                        <input type="text" class="form-control" placeholder="Universitas">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="date">Tanggal dimulai</label>
                        <input class="form-control datepicker" id="date" name="date" placeholder="MM/DD/YYY" type="text" data-provide="datepicker" data-date-format="mm/dd/yyyy">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="date">Tanggal selesai</label>
                        <input class="form-control datepicker" id="date" name="date" placeholder="MM/DD/YYY" type="text" data-provide="datepicker" data-date-format="mm/dd/yyyy"/>
                    </div>
                    <button class="btn btn-primary " name="submit" type="submit">Submit</button>
                </form>
            </div>
        </center>
    </div>
    <script>
        $(function(){
            $(".datepicker").datepicker({
                format: 'mm/dd/yyyy',
                startDate:'-3d'
            });
        });
    </script>
@endsection