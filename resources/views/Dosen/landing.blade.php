@extends('templatedosen.navbar2')
@section('title','Sistem Informasi Kampus')
@section('content')
<div class="container">
    <div class="searchbar">
        <label class="dosensearchbarlabel" for="caridosen">Cari Data Dosen</label>
        <form method="post" action="#">
            @csrf
            <div class="form-group"> <!-- Date input -->
                <input class="form-control" placeholder="NIDN Dosen" id="nidndosen" name="nidndosen" type="text">
            </div>
        </form>
    </div>
    <div class="lsitdosen">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">NIP</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Gelar</th>
                    {{-- <th scope="col">Pendidikan</th> --}}
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $dsn)
                <tr>
                    <td scope="col">{{ $dsn -> nip }}</td>
                    <td scope="col">{{ $dsn ->  nama }}</td>
                    <td scope="col">{{ $dsn -> gelar }}</td>
                    {{-- <td scope="col">{{ $dosen -> 'Gelar' }}</td> --}}
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="d-flex justify-content-center">
          {{ $data ->links('pagination::bootstrap-4') }}
        </div>
    </div>
</div>
@endsection