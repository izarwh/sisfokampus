<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- jQuery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/logindosen.css') }}" rel="stylesheet" type="text/css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Dosen</title>
</head>
<body>
    <div class="container">
        <div class="row content flex-column col-md-3">
            <center>
                <div class="class-md-6">
                    <img class="login__images" src="{{ asset('img/573.jpg') }}" alt="">
                </div>
            </center>
            <div class="col-md-12">
                <h3 class="signin-text col-md-12">Log in Dosen</h3>
                <center>
                    <form action="{{ route('authlogindosen') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label class="siginin-text row" for="nip">NIP</label>
                            <input class="form-control" type="Text" name="nip">
                        </div>
                        <div class="form-group">
                            <label class="siginin-text row" for="Password">Password</label>
                            <input class="form-control" type="password" name="password">
                        </div>
                        <center>
                            <button type="submit" class="btn btn-logindosen">log in</button>
                        </center>
                    </form>
                </center>
            </div>
            <div class="row">
                @if(session('errors'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Something it's wrong:
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif
                    @if (Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif
            </div>
        </div>
    </div>
</body>
</html>