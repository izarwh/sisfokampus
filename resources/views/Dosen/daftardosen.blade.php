@extends('Templatedosen.navbar2')
@section('title','daftardosen')
@section('content')
    <div class="container">
        <div class="searchbar">
            <label class="dosensearchbarlabel" for="caridosen">Cari Data Dosen</label>
            <form method="post" action="#">
                @csrf
                <div class="form-group"> <!-- Date input -->
                    <input class="form-control" placeholder="NIDN Dosen" id="nidndosen" name="nidndosen" type="text">
                </div>
            </form>
        </div>
        <div class="lsitdosen">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Gelar</th>
                        <th scope="col">Pendidikan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th></th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection