@extends('templatedosen.navbar2')
@section('title','Sistem Informasi Kampus')
@section('content')

    <div class="container">
        <div class="card pad__card">
            <h1 class="name__pad">{{ $user->nama }}</h1>
            <p class="title">Dosen</p>
            @foreach($riwayat as $riwayat)
            <p>{{ $riwayat->strata }}{{ ' ' }}{{ $riwayat->jurusan }}</p>
            <p>{{ $riwayat->sekolah }}{{ ' ' }}{{ $riwayat->tahunMulai }}{{ '-' }}{{ $riwayat->tahunSelesai }}</p>
            @endforeach
            <center>
                <a href="{{ Route('tambahriwayat') }}" class="btn btn-primary">Tambah Riwayat</a>
            </center>
            <a href="#"><i class="fa fa-dribbble"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a class="btn btn-dark btn__pad" href="{{ route('editprofiledosen') }}">Edit</a>
          </div>
    </div>
@endsection