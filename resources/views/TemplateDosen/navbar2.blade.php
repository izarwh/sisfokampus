<!doctype html>
<html>

<head>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/nav2.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/profile.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/searchdosen.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />
    <title>@yield('title')</title>

    <!-- Bootstrap Date-Picker Plugin -->
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />

</head>

<body>
    <div class="wrapper">
        <nav class="col-md-3" id="sidebar">
            <div class="sidebar-header">
                <h3>Sistem Informasi Kampus</h3>
            </div>
            <ul class="lisst-unstyled components">
                <div class="row-sm-2">
                    <p>{{ $user->nama }}</p> 
                </div>
                <li>
                    <a href="{{ route('landing') }}">Home</a>
                </li>
                <li>
                    <a href="{{ route('profiledosen') }}">Profile</a>
                </li>
                <li>
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false"
                        class="dropdown-toggle">Menu</a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li>
                            <a href="#">Edit Riwayat</a>
                        </li>
                        <li>
                            <a href="{{ route('landing') }}">Lihat Daftar Dosen</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">Policy</a>
                </li>
                <li>
                    <a href="#">Contact Us</a>
                </li>
            </ul>
        </nav>
        <div class="col-2 toggler" id="content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light nav__togler">
                <div class="container">
                    <button type="button" id="sidebarCollapse" class="btn btn-toggler">
                        <span><i class="far fa-arrow-alt-circle-right"></i></span>
                    </button>
                </div>
            </nav>
        </div>
        @yield('content')
    </div>
    <script>
        $(document).ready(function() {
            $('#sidebarCollapse').on('click', function() {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>
